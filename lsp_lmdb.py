#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import sys
import shutil
import os
import glob
import caffe
import lmdb
import numpy as np
import cv2 as cv
from scipy.io import loadmat

def file(data_dir):
	flic_base = []
	for fname in glob.glob('%s/*.jpg' % data_dir):
		flic_base.append(os.path.basename(fname).split('.')[0])
		#print fname

#main
if __name__ == '__main__':
	db = 'valid'
	data_dir = '../data/LSP/images'
	out_dir = '%s/lsp_lmdb' % data_dir

	if not os.path.exists(out_dir):
		print 'create out_dir'
		os.makedirs(out_dir)
	else:
		print 'path exits : %s' % (out_dir)

	#db
	env = lmdb.Environment(out_dir, map_size=1099511627776)
	txn = env.begin(write=True, buffers=True)
	
	#create keys
	keys = np.arange(20000000)
	np.random.shuffle(keys)

	n_patches = 0

	for flic_fn in glob.glob('%s/*.jpg' % data_dir):
		flic_im = cv.imread(flic_fn, cv.IMREAD_COLOR)
		#print 'flic_fn : %s' % flic_fn
		#print 'flic_im.shape : ', flic_im.shape
		#print flic_im.shape
		
		datum = caffe.io.array_to_datum(flic_im, 0)
		joints = loadmat('LSP/joints.mat')
		joints = joints['joints'].swapaxes(0, 2)
		#print 'joints.shape : ', joints.shape
		datum.float_data.extend(joints.flatten().tolist())
		key = '%010d' % keys[n_patches]
        	value = datum.SerializeToString()
        	txn.put(key, value)
		
		n_patches += 1
		
		if n_patches %10000 == 0:
			print 'patches:\t', n_patches
			txn.commit()
			txn = env.begin(write=True, buffers=True)
	txn.commit()
    	env.close()
    	print 'patches:\t', n_patches
    	print 'Done'# This is my LSP_LMDB
