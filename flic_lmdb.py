#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import sys
import shutil
import os
import glob
import caffe
import lmdb
import numpy as np
import cv2 as cv

def file(data_dir):
	flic_base = []
	for fname in glob.glob('%s/*.jpg' % data_dir):
		flic_base.append(os.path.basename(fname).split('.')[0])
		#print fname

#main
if __name__ == '__main__':
	db = 'valid'
	data_dir = '../data/FLIC-full/images'
	out_dir = '%s/flic_lmdb' % data_dir

	#db
	env = lmdb.Environment(out_dir, map_size=1099511627776)
	txn = env.begin(write=True, buffers=True)
	
	for flic_fn in glob.glob('%s/*.jpg' % data_dir):
		flic_im = cv.imread(flic_fn, cv.IMREAD_COLOR)
		#print 'flic_fn : %s' % flic_fn
		#print 'flic_im.shape : ', flic_im.shape
		
		datum = caffe.io.array_to_datum(flic_im, 0)
		datum.float_data.extend(map_patch.flatten().tolist()) #edit for joint-coordinate
		key = '%010d' % keys[n_patches]
        value = datum.SerializeToString()
        txn.put(key, value)
		
		n_patches += 1
		
		if n_patches %10000 == 0:
			print 'patches:\t', n_patches
			txn.commit()
			txn = env.begin(write=True, buffers=True)
	txn.commit()
    env.close()
    print 'patches:\t', n_patches

#ika mihenshu
	datum = caffe.io.caffe_pb2.Datum()
	if not os.path.exists('%s/lmdb/%s.lmdb' % (data_dir, db)):
		print "path don't exist"
		sys.exit()
	env = lmdb.open('%s/lmdb/%s.lmdb' % (data_dir, db))
	txn = env.begin(write=False, buffers=False)
	cur = txn.cursor()
	cur.next()
	print '%s/lmdb/%s.lmdb' % (data_dir, db)
	for i in range(1000):
		key, value = cur.item()
		print i, '\tkey:', key

		datum.ParseFromString(value)
		data = [struct.unpack('B', d) for d in datum.data]
		data = np.asarray(data, dtype=np.uint8)
		data = data.reshape((datum.channels, datum.height, datum.width))
		sat = data.swapaxes(0, 2).swapaxes(0, 1)
		map = np.asarray(datum.float_data).reshape((16, 16))
		cmap = np.copy(sat)
		cmap[24:40, 24:40, 0] = np.asarray(map == 0, dtype=np.uint8) * 255
		cmap[24:40, 24:40, 1] = np.asarray(map == 1, dtype=np.uint8) * 255
		cmap[24:40, 24:40, 2] = np.asarray(map == 2, dtype=np.uint8) * 255
		cv.imwrite('%s/%d_%s.png' %
			(out_dir, i, key), np.hstack((sat, cmap)))
		cur.next()